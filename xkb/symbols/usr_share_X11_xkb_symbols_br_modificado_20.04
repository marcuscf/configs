default
xkb_symbols "abnt2" {

    // Based on a very simple Brazilian ABNT2 keyboard,
    // by Ricardo Y. Igarashi (iga@that.com.br).
    // With added support for dead keys in I18N applications,
    // by Conectiva  (http://www.conectiva.com.br).

    // Modificado por Marcus Aurelius, para adicionar aspas curvas, travessão e outros símbolos
    // (minhas linhas diferentes às vezes são identificáveis pela indentação e alinhamento).

    include "latin"
    name[Group1]="Portuguese (Brazil)";

    key <TLDE>  { [apostrophe,   quotedbl,             plusminus,     dead_doubleacute ] };
    key <AE02>  { [         2,         at,           twosuperior,          musicalflat ] };
    key <AE03>  { [         3, numbersign,         threesuperior,         musicalsharp ] };
    key <AE04>  { [         4,     dollar,              sterling,        lessthanequal ] };
    key <AE05>  { [         5,    percent,                  cent,     greaterthanequal ] };
    key <AE06> { [            6, dead_diaeresis,       notsign,       diaeresis ] };
    key <AE07>  { [         7,  ampersand,              Greek_pi,            identical ] };
    key <AE08>  { [         8,   asterisk,    enfilledcircbullet,             infinity ] };

    // Teclas com parênteses (, ) merecem aspas curvas também “, ”; ‘, ’
    key <AE09>  { [         9,  parenleft,   leftdoublequotemark,  leftsinglequotemark ] };
    key <AE10>  { [         0, parenright,  rightdoublequotemark, rightsinglequotemark ] };
    // Tecla com -, _ merece outros traços, ficando -, _, –, —
    key <AE11>  { [     minus, underscore,                endash,               emdash ] };
    // Tecla com = merece ≠
    key <AE12>  { [     equal,       plus,               section,             notequal ] };

    key <AD01>  { [         q,          Q,                 slash,                U017F ] }; // U+017F is Long S
    // Tecla com ? merece ¿
    key <AD02>  { [         w,          W,              question,         questiondown ] };
    key <AD03>  { [         e,          E,                degree,             EuroSign ] };
    key <AD04>  { [         r,          R,                 U2030,           registered ] }; // U+2030 is Permille

    key <AC02>  { [         s,          S,                ssharp,                U0283 ] }; // U+0283 is Latin Small Letter Esh
    key <AC04>  { [         f,          F,               dstroke,          Greek_theta ] };
    // ENG vai para a tecla N, podemos colocar EZH aqui no G:
    key <AC05>  { [         g,          G,                   ezh,                  EZH ] };

    key <AD11> { [   dead_acute,     dead_grave,         acute,           grave ] };
    key <AD12> { [  bracketleft,      braceleft,   ordfeminine,     dead_macron ] };

    // a tecla ], }, º merece uma bolinha combinável ˚ (dead_abovering)
    key <BKSL>  { [ bracketright,     braceright,      masculine,       dead_abovering ] };

    // Dispensaremos dead_hook e dead_horn da tecla J.
    key <AC07>  { [         j,          J,             checkmark,          ballotcross ] };
    key <AC08>  { [         k,          K,                   kra,             approxeq ] };

    // Removi dead_ogonek da tecla =, + vou adicionar a Alt+Shift+Ç. O dead_doubleacute eu já pus em ', " e o dead_acute já existe no leiaute brasileiro mesmo.
    key <AC10>  { [     ccedilla,       Ccedilla,   dead_cedilla,          dead_ogonek ] };
    key <AC11> { [   dead_tilde,dead_circumflex,    asciitilde,     asciicircum ] };

    key <LSGT> { [    backslash,            bar,    dead_caron,      dead_breve ] };

    // AltGr+Shift+Z e AltGr+Shift+X estavam com os símbolos <, > repetidos. Podemos colocar alguns símbolos melhores.
    // É útil ter uma cópia de \, | para teclados sem a tecla extra entre Shift e Z (provavelmente por isso <, > estavam aqui,
    // já que leiautes europeus costumam usar essa tecla para os símbolos <, >).
    // ˘ merece estar repetido para não ficar só na tecla \, |; enquanto que ˇ não estava no teclado brasileiro.
    // Sobre a repetição de «, » (pois adicionarei também às teclas ,<, .> à direita do M), ainda não decidi o que é melhor.
    key <AB01>  { [         z,          Z,             backslash,            paragraph ] };
    key <AB02>  { [         x,          X,                   bar,            brokenbar ] };
    key <AB03>  { [         c,          C,                 U20A2,            copyright ] }; // U+20A2 is Cruzeiro Sign
    key <AB04>  { [         v,          V,            dead_caron,                U266B ] }; // U+266B is Beamed Eighth Notes
    key <AB05>  { [         b,          B,            dead_breve,                U266E ] }; // U+266E is Music Natural Sign
    key <AB06>  { [         n,          N,                   eng,                  ENG ] };
    key <AB07>  { [         m,          M,                    mu,          Greek_OMEGA ] };

    // Teclas com <, > merecem «, »; e o periodcentered pode ser movido para o lado.
    key <AB08> { [     comma,       less,         guillemotleft,             multiply ] };
    key <AB09> { [    period,    greater,        guillemotright,             division ] };
    key <AB10> { [ semicolon,      colon,        periodcentered,        dead_abovedot ] };
    // Valor default de <AB10> abaixo:
    //key <AB10> { [    semicolon,          colon, dead_belowdot,   dead_abovedot ] };

    // The ABNT-2 keyboard has this special key:
    key <AB11> { [        slash,       question,        degree,    questiondown ] };

    // Desativado porque causa pequenos engasgos no Gnome.
    //modifier_map Mod3   { Scroll_Lock };

    include "kpdl(comma)"

    include "level3(ralt_switch)"
};


partial alphanumeric_keys
xkb_symbols "nodeadkeys" {

    include "br(abnt2)"
    name[Group1]="Portuguese (Brazil, no dead keys)";

    key <AE06> { [            6,      diaeresis,       notsign,         notsign ] };
    key <AD11> { [   apostrophe,          grave                                 ] };
    key <AC10> { [     ccedilla,       Ccedilla,         acute,     doubleacute ] };
    key <AC11> { [   asciitilde,    asciicircum                                 ] };
    key <AB10> { [    semicolon,          colon, dead_belowdot,        abovedot ] };
};


// The ABNT2 keyboard on IBM/Lenovo Thinkpads,
// by Piter PUNK <piterpk@terra.com.br>.
//
partial alphanumeric_keys
xkb_symbols "thinkpad" {

    include "br(abnt2)"
    name[Group1]="Portuguese (Brazil, IBM/Lenovo ThinkPad)";

    key <RCTL> { [        slash,       question,        degree,    questiondown ] };
};


partial alphanumeric_keys
xkb_symbols "olpc" {

    include "us(basic)"
    name[Group1]="Portuguese (Brazil)";

    key <TLDE> { [   apostrophe,        quotedbl                                ] };
    key <AE01> { [            1,          exclam,   onesuperior                 ] };
    key <AE02> { [            2,              at,   twosuperior                 ] };
    key <AE03> { [            3,      numbersign, threesuperior                 ] };
    key <AE04> { [            4,          dollar,      sterling                 ] };
    key <AE05> { [            5,         percent,          cent                 ] };
    key <AE06> { [            6,  dead_diaeresis,       notsign                 ] };
    key <AE12> { [        equal,            plus,       section                 ] };

    key <AD03> { [            e,               E,      EuroSign                 ] };
    key <AD11> { [   dead_acute,      dead_grave,         acute,          grave ] };
    key <AD12> { [  bracketleft,       braceleft,     0x10000AA                 ] };
    key <BKSL> { [ bracketright,      braceright,     0x10000BA                 ] };

    key <AC10> { [     ccedilla,        Ccedilla                                ] };
    key <AC11> { [   dead_tilde, dead_circumflex                                ] };

    key <AB01> { [            z,               Z,           bar                 ] };
    key <AB03> { [            c,               C,     0x10020A2                 ] };
    key <AB09> { [       period,         greater,     backslash                 ] };
    key <AB10> { [    semicolon,           colon                                ] };

    key <I219> { [        slash,        question,        degree, ISO_Next_Group ] };

    include "level3(ralt_switch)"
};


//
// Brazilian Dvorak layout                                 2005-04-18
// "Teclado Simplificado Brasileiro" ou "Dvorak Brasileiro"
//
// Heitor Moraes    heitor.moraes@gmail.com
// Luiz Portella    lfpor@lujz.org
// Nando Florestan  nando2003@mandic.com.br
// Ari Caldeira     ari@tecladobrasileiro.com.br
//
partial alphanumeric_keys
xkb_symbols "dvorak" {

    name[Group1]="Portuguese (Brazil, Dvorak)";

// Numeric row
    key <TLDE> { [   apostrophe,        quotedbl,           dead_caron, dead_doubleacute ] };
    key <AE01> { [            1,          exclam,          onesuperior,       exclamdown ] };
    key <AE02> { [            2,              at,          twosuperior,          onehalf ] };
    key <AE03> { [            3,      numbersign,        threesuperior,    threequarters ] };
    key <AE04> { [            4,          dollar,             sterling,       onequarter ] };
    key <AE05> { [            5,         percent,                 cent,       0x01002030 ] };
    key <AE06> { [            6,  dead_diaeresis,              notsign,        diaeresis ] };
    key <AE07> { [            7,       ampersand,        dead_belowdot,    dead_abovedot ] };
    key <AE08> { [            8,        asterisk,          dead_ogonek,        dead_horn ] };
    key <AE09> { [            9,       parenleft,         dead_cedilla,        dead_hook ] };
    key <AE10> { [            0,      parenright,          dead_macron,       dead_breve ] };
    key <AE11> { [  bracketleft,       braceleft,          ordfeminine,       0x01000326 ] };
    key <AE12> { [ bracketright,      braceright,            masculine,   dead_abovering ] };


// Upper row
    key <AD01> { [        slash,        question,               degree,     questiondown ] };
    key <AD02> { [        comma,            less,           0x01000329,       0x01000313 ] };
    key <AD03> { [       period,         greater,           0x01002022,   periodcentered ] };
    key <AD04> { [            p,               P,                thorn,            THORN ] };
    key <AD05> { [            y,               Y,                  yen,              yen ] };
    key <AD06> { [            f,               F,  leftdoublequotemark,  leftsinglequotemark ] };
    key <AD07> { [            g,               G, rightdoublequotemark, rightsinglequotemark ] };
    key <AD08> { [            c,               C,              uparrow,        copyright ] };
    key <AD09> { [            r,               R,           registered,       registered ] };
    key <AD10> { [            l,               L,              lstroke,          Lstroke ] };
    key <AD11> { [   dead_acute,      dead_grave,                acute,            grave ] };
    key <AD12> { [        equal,            plus,              section,        plusminus ] };

// Central row
    key <AC01> { [            a,               A,                   ae,               AE ] };
    key <AC02> { [            o,               O,                   oe,               OE ] };
    key <AC03> { [            e,               E,             EuroSign,         EuroSign ] };
    key <AC04> { [            u,               U,               oslash,         Ooblique ] };
    key <AC05> { [            i,               I,             idotless,        Iabovedot ] };
    key <AC06> { [            d,               D,                  eth,              ETH ] };
    key <AC07> { [            h,               H,            leftarrow,        paragraph ] };
    key <AC08> { [            t,               T,           rightarrow,        trademark ] };
    key <AC09> { [            n,               N,                  eng,              ENG ] };
    key <AC10> { [            s,               S,               ssharp,            U1E9E ] };
    key <AC11> { [   dead_tilde, dead_circumflex,           asciitilde,      asciicircum ] };
    key <BKSL> { [        minus,      underscore,           0x01002015,       0x01000336 ] };

// Lower row
    key <LSGT> { [     ccedilla,        Ccedilla,            backslash,              bar ] };
    key <AB01> { [    semicolon,           colon,           0x01000331,       0x0100032D ] };
    key <AB02> { [            q,               Q,           0x01000259,       0x0100018F ] };
    key <AB03> { [            j,               J,           0x01000292,       0x010001B7 ] };
    key <AB04> { [            k,               K,        guillemotleft,       0x01002039 ] };
    key <AB05> { [            x,               X,             multiply,         division ] };
    key <AB06> { [            b,               B,       guillemotright,       0x0100203A ] };
    key <AB07> { [            m,               M,            downarrow,               mu ] };
    key <AB08> { [            w,               W,               ubreve,           Ubreve ] };
    key <AB09> { [            v,               V,   doublelowquotemark, singlelowquotemark ] };
    key <AB10> { [            z,               Z,           0x0100201F,       0x0100201B ] };
    key <AB11> { [    backslash,             bar,             currency,        brokenbar ] };

    key <SPCE> { [        space,           space,         nobreakspace,     nobreakspace ] };

// Configures the "," for the numeric keypad
    include "kpdl(comma)"

// Configures the use of the AltGr key
    include "level3(ralt_switch)"
};


//
// Brazilian Nativo layout.
//     This is a Dvorak-based layout, designed for the Portuguese language.
//
// Ari Caldeira    <ari@tecladobrasileiro.com.br>    2005-07-19
//
partial alphanumeric_keys
xkb_symbols "nativo" {

    name[Group1]="Portuguese (Brazil, Nativo)";

// Numeric row
    key <TLDE> { [        equal,            plus,              section,        plusminus ] };
    key <AE01> { [            1,          exclam,          onesuperior,       exclamdown ] };
    key <AE02> { [            2,              at,          twosuperior,          onehalf ] };
    key <AE03> { [            3,      numbersign,        threesuperior,    threequarters ] };
    key <AE04> { [            4,          dollar,             sterling,       onequarter ] };
    key <AE05> { [            5,         percent,                 cent,       0x01002030 ] };
    key <AE06> { [            6,  dead_diaeresis,              notsign,        diaeresis ] };
    key <AE07> { [            7,       ampersand,        dead_belowdot,    dead_abovedot ] };
    key <AE08> { [            8,        asterisk,          dead_ogonek,        dead_horn ] };
    key <AE09> { [            9,       parenleft,         dead_cedilla,        dead_hook ] };
    key <AE10> { [            0,      parenright,          dead_macron,       dead_breve ] };
    key <AE11> { [  bracketleft,       braceleft,          ordfeminine,       0x01000326 ] };
    key <AE12> { [ bracketright,      braceright,            masculine,   dead_abovering ] };

// Upper row
    key <AD01> { [        slash,        question,               degree,     questiondown ] };
    key <AD02> { [        comma,            less,           0x01000329,       0x01000313 ] };
    key <AD03> { [       period,         greater,           0x01002022,   periodcentered ] };
    key <AD04> { [            h,               H,            paragraph,        paragraph ] };
    key <AD05> { [            x,               X,             multiply,         division ] };
    key <AD06> { [            w,               W,               ubreve,           Ubreve ] };
    key <AD07> { [            l,               L,              lstroke,          Lstroke ] };
    key <AD08> { [            t,               T,            trademark,        trademark ] };
    key <AD09> { [            c,               C,            copyright,        copyright ] };
    key <AD10> { [            p,               P,                thorn,            THORN ] };
    key <AD11> { [   dead_tilde, dead_circumflex,           asciitilde,      asciicircum ] };
    key <AD12> { [        minus,      underscore,           0x01002015,       0x01000336 ] };

// Central row
    key <AC01> { [            i,               I,             idotless,        Iabovedot ] };
    key <AC02> { [            e,               E,             EuroSign,         EuroSign ] };
    key <AC03> { [            a,               A,                   ae,               AE ] };
    key <AC04> { [            o,               O,                   oe,               OE ] };
    key <AC05> { [            u,               U,               oslash,         Ooblique ] };
    key <AC06> { [            m,               M,                   mu,               mu ] };
    key <AC07> { [            d,               D,                  eth,              ETH ] };
    key <AC08> { [            s,               S,               ssharp,            U1E9E ] };
    key <AC09> { [            r,               R,           registered,       registered ] };
    key <AC10> { [            n,               N,                  eng,              ENG ] };
    key <AC11> { [   dead_acute,      dead_grave,                acute,            grave ] };
    key <BKSL> { [   apostrophe,        quotedbl,           dead_caron, dead_doubleacute ] };

// Lower row
    key <LSGT> { [    semicolon,           colon,           0x01000331,       0x0100032D ] };
    key <AB01> { [            y,               Y,                  yen,              yen ] };
    key <AB02> { [     ccedilla,        Ccedilla,            backslash,              bar ] };
    key <AB03> { [            j,               J,           0x01000292,       0x010001B7 ] };
    key <AB04> { [            b,               B,        guillemotleft,       0x01002039 ] };
    key <AB05> { [            k,               K,       guillemotright,       0x0100203A ] };
    key <AB06> { [            q,               Q,           0x01000259,       0x0100018F ] };
    key <AB07> { [            v,               V,   doublelowquotemark,   singlelowquotemark ] };
    key <AB08> { [            g,               G,  leftdoublequotemark,  leftsinglequotemark ] };
    key <AB09> { [            f,               F, rightdoublequotemark, rightsinglequotemark ] };
    key <AB10> { [            z,               Z,           0x0100201F,       0x0100201B ] };
    key <AB11> { [    backslash,             bar,             currency,        brokenbar ] };

    key <SPCE> { [        space,           space,         nobreakspace,     nobreakspace ] };

// Configures the "," for the numeric keypad
    include "kpdl(comma)"

// Configures the use of the AltGr key
    include "level3(ralt_switch)"
};


//
// Brazilian Nativo layout for US keyboards.
//
// Ari Caldeira    <ari@tecladobrasileiro.com.br>    2005-07-19
//
partial alphanumeric_keys
xkb_symbols "nativo-us" {

    include "br(nativo)"

    name[Group1]="Portuguese (Brazil, Nativo for US keyboards)";

    key <AB01> { [            y,               Y,              ccedilla,        Ccedilla ] };
    key <AB02> { [    semicolon,           colon,             backslash,             bar ] };
};


//
// Brazilian Nativo layout for typing Esperanto.
//
// Ari Caldeira    <ari@tecladobrasileiro.com.br>    2005-07-19
//
partial alphanumeric_keys
xkb_symbols "nativo-epo" {

    include "br(nativo)"

    name[Group1]="Esperanto (Brazil, Nativo)";

    key <AD04> { [            h,               H,          hcircumflex,      Hcircumflex ] };
    key <AD05> { [  ccircumflex,     Ccircumflex,                    x,                X ] };
    key <AD06> { [       ubreve,          Ubreve,                    w,                W ] };

    key <AB01> { [  jcircumflex,     Jcircumflex,                    y,                Y ] };
    key <AB02> { [  scircumflex,     Scircumflex,             ccedilla,         Ccedilla ] };
    key <AB06> { [  gcircumflex,     Gcircumflex,                    q,                Q ] };
};

// EXTRAS:

partial alphanumeric_keys
	xkb_symbols "sun_type6" {
	include "sun_vndr/br(sun_type6)"
};
