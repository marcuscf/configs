# Configurações diversas

Este projeto contém algumas configurações que eu costumo usar e que podem ser úteis em mais de um computador ou para outras pessoas.

Em `ms_keyboard_layout_creator`, há um layout de teclado brasileiro com vários símbolos e acentos que não existem no teclado normal.

Em `xkb` há algumas configurações semelhantes para o teclado para Linux, mas um pouco mais reduzidas.