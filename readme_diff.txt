Add this to your .gitconfig, so that utf-16 files are converted to utf-8
before the diff operation:

[diff "utf-16"]
	textconv = "iconv -f utf-16 -t utf-8"

The .gitattributes file specifies which files are marked with the utf-16
attribute.
