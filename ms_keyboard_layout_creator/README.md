# Layout de teclado brasileiro multilíngüe

Este layout de teclado (ou leiaute) pode ser instalado em qualquer computador com Windows (funciona com Windows XP, Vista, 7, 8, 10) e com teclado brasileiro para acrescentar mais símbolos e acentos, permitindo digitar em dezenas de línguas e facilitando o acesso a símbolos que normalmente só estariam disponíveis no mapa de caracteres ou alguma outra ferramenta.

Você já se perguntou por que não tem o travessão — direto no teclado? A linha baixa _ não serve. O hífen - até quebra um galho se colocar espaços ao redor, mas não é a mesma coisa. Ou talvez você prefira a meia-risca –, que não é tão longo quanto o travessão.

Cadê o símbolo do Euro € no teclado brasileiro? E as “aspas curvas”? Por que depender da conversão automática que ocorre em apenas alguns programas? Talvez você goste também de escrever «assim» ou usar mais símbolos matemáticos como ≠, π, µ (como em µm, micrômetro) ou flechas ←↓↑→.

Com este layout é possível digitar tudo isso sem alterar a disposição do que já está no teclado brasileiro, usando a tecla AltGr (Alt da direita). Além disso, acentos e letras que antes não se combinavam, agora funcionam: é possível digitar ĉ para escrever em esperanto, ć para polonês e ŕ para eslovaco.

¿E podemos fazer perguntas em espanhol? ¡Podemos sim!

## Resumo da disposição dos símbolos

Esta é uma visão geral de todo o teclado, com comentários à direita indicando algumas combinações que são possíveis com os acentos da linha.

Cada tecla tem 4 caracteres, de acordo com a combinação de Shift e AltGr que você usar.  
A disposição de cada tecla segue o diagrama abaixo, com a letra _normal_ (sem Shift nem AltGr) no canto inferior esquerdo de cada bloco.

```
Shift   AltGr+Shift  
Normal  AltGr
```

### Versão 2016

```
"˝	!¡	@|	#♯	$≤	%≥	¨≈	&≡	*∞	(‘	)’	_—	+≠  
'\	1¹	2²	3³	4£	5¢	6¬	7π	8Ω	9“	0”	-–	=§

Qſ	W¿	E€	R®	TÞ	YÜ	Uθ	I•	OØ	PŒ	`Ɔ	{¯	// àǜỳẁ, āēīōūǖȳ ḡ  
q/	w?	e°	r€	tþ	yü	u˘	iı	oø	pœ	´ɔ	[ª	// ăĕĭŏŭ ğ, áǘẃ ćśź

AÆ	SƩ	DÐ	F♪	GƷ	H‰	J✗	K∈	L∉	Ç˛	^Ə	}˚	// ąę, âŷŵ ĉĝĥĵŝ, åů  
aæ	sʃ	dð	f¥	gʒ	h←	j↓	k↑	l→	ç¸	~ə	]º	// ãũỹ, Ģģşţ  

|±	Zß	X·	C©	V♫	B♮	NŊ	M✓	<,	>˙	:Ɛ	?¿	// șț, ȧ ċṡż  
\−	z÷	x×	c₢	vˇ	b♭	nŋ	mµ	,«	.»	;ɛ	/°	// ǎǚ čšž 
```

### Versão 2021

```
"˝	!¡	@♭	#♯	$≤	%≥	¨‹	&›	*∞	(‘	)’	_—	 +≠  
'±	1¹	2²	3³	4£	5¢	6¬	7π	8•	9“	0”	-–	 =§  

Qſ	W¿	E€	R®	TÞ	Y¥	U↑	Iı	OØ	PŒ	`Ɔ	{¯	// ` ¯ geram: àǜỳẁ, āēīōūǖȳ ḡ  
q/	w?	e°	r‰	tþ	y←	u↓	i→	oø	pœ	´ɔ	[ª	// ´ gera: áǘýẃ ćśź  

AÆ	Sʃ	DÐ	Fθ	GƷ	H♪	J✗	K,	L˙	Ç˛	^Ə	}˚	// , ˙ ˛ ^ ° geram: șț, ȧ ċṡż, ąę, âŷŵ ĉĝĥĵŝ, åů  
aæ	sß	dð	fđ	gʒ	h−	j✓	kĸ	l·	ç¸	~ə	]º	// ~ ¸ geram: ãõũỹ ñ, Ģģşţ  

|˘	Z¶	X¦	C©	V♫	B♮	NŊ	MΩ	<×	>÷	:Ɛ	?¿	// ¦ gera: ƀ đ ǥ ħ ɨ ɉ ł ɍ ŧ ʉ ɏ ƶ ø  
\−	z\	x|	c₢	vˇ	b˘	nŋ	mµ	,«	.»	;ɛ	/°	// ˇ ˘ geram: ǎǚ čšž, ăĕĭŏŭ ğ
```

## Símbolos e acentuação

### Símbolos monetários
€ (AltGr+Shift+e) { Euro. O AltGr+E está ocupado então use o AltGr+Shift }  
¥ { Yen. Está próximo aos outros símbolos monetários, a posição varia conforme a versão do layout }  
\$ £ ¢  { Já presentes no layout brasileiro, nas teclas “4” e “5” }  
₢ (AltGr+c) { Antigo símbolo de Cruzeiro, já presente no layout brasileiro, nunca vi ninguém usar, só conhecia o Cr$ mesmo }

### Flechas

#### Versão 2016

{ Mnemônico: correspondem às teclas de flecha do editor de texto Vi/Vim }

← (AltGr+h)  
↓ (AltGr+j) { Mnemônico: j é uma letra que desce para baixo da linha, por isso ↓ }  
↑ (AltGr+k) { Mnemônico: k é uma letra alta, por isso ↑ }  
→ (AltGr+l)  

#### Versão 2021

← (AltGr+y)  
↓ (AltGr+u)  
↑ (AltGr+Shift+U)  
→ (AltGr+i)  

### Pontuação e outros símbolos
– (AltGr+-) { Meia-risca. Mais comprida que o hífen e menor que o travessão }  
— (AltGr+Shift+-) { Travessão. Mais comprido que o hífen e a meia-risca }  
“ ” (AltGr+9 | AltGr+0) { Aspas curvas. Mnemônico: os parênteses sobre os dígitos 9 e 0 }  
‘ ’ (AltGr+Shift+9 | AltGr+Shift+0) { Aspas simples curvas }  
« » (AltGr+, | AltGr+.) { Aspas francesas. Mnemônico: «» estão nas teclas <> }  
® (AltGr+Shift+R)  
© (AltGr+Shift+C)  
• { Bolinha. Útil para itens de lista }  
¡ (AltGr+Shift+1) { Início de exclamação em espanhol. Na mesma tecla da exclamação normal }  
¿ (AltGr+Shift+/ | AltGr+Shift+W) { Início de interrogação em espanhol. Nas mesmas teclas onde está a interrogação normal }  
✓ { Check mark, marcação "OK" }  
✗ { Marcação X, marcação de cédula de votação, marcação "Errado" }  
♭ { Bemol }  
♯ { Sustenido }  

### Símbolos matemáticos

− (AltGr+\\) { Sinal de menos. Em algumas fontes é um pouco maior que o hífen }  
± { + ou - }  
× { Multiplicação }  
÷ { Divisão }  
· { Multiplicação }  
≠ (AltGr+Shift+=) { Diferente }  
≤ (AltGr+Shift+4) { Menor ou igual a }  
≥ (AltGr+Shift+5) { Maior ou igual a }  
µ (AltGr+m) { Micro, prefixo de unidades SI }  
¬ (AltGr+6) { Negação (já existente no teclado) }  
π (AltGr+7) { Pi }  
Ω { Ohm }  
∞ (AltGr+Shift+8) { Infinito }  
Å (AltGr+Shift+] A) { Ångström }  
% { Porcento (já existente no teclado) }  
‰ { Por mil }  

### Diacríticos

Digite qualquer um dos diacríticos abaixo como um acento normal (nada aparecerá na tela) e depois digite a letra que você deseja acentuar.

#### Trema: ¨ (Shift+6)  

`ä ë ḧ ï ö ẗ ü ẅ ẍ ÿ`  
`Ä Ë Ḧ Ï Ö   Ü Ẅ Ẍ Ÿ`

Na versão 2026, o ü pode ser digitado também com AltGr+y, permitindo assim acentuar o ü, como em ǘ. Combinações como essa são usadas na romanização Pinyin do chinês mandarim.  
{ A combinação AltGr+y vem do teclado US International }

Na versão 2021, ǖ, ǘ, ǜ, ǚ, do Pinyin podem ser obtidos com um dos tons ¯, ´, `, ˇ seguido da tecla v.

#### Til: ~

`ã ẽ ĩ ñ õ ũ ṽ ỹ`  
`Ã Ẽ Ĩ Ñ Õ Ũ Ṽ Ỹ`

#### Circunflexo: ^ (Shift+~)

Além de poder usar sobre as vogais, é possível utilizá-lo, por exemplo, sobre consoantes para digitar em Esperanto, com ĉ, ĝ, ĥ, ĵ, e ŝ.

`â ĉ ê ĝ ĥ î ĵ ô ŝ û ŵ ŷ ẑ`  
`Â Ĉ Ê Ĝ Ĥ Î Ĵ Ô Ŝ Û Ŵ Ŷ Ẑ`

#### Agudo: ´

`á ć é ǵ í ḱ ĺ ḿ ń ó ṕ ŕ ś ú ǘ ẃ ý ź ḉ ǽ ǿ`  
`Á Ć É Ǵ Í Ḱ Ĺ Ḿ Ń Ó Ṕ Ŕ Ś Ú Ǘ Ẃ Ý Ź Ḉ Ǽ Ǿ`

#### Grave: \` (Shift+´)

`à è ì ǹ ò ù ẁ ỳ ǜ`  
`À È Ì Ǹ Ò Ù Ẁ Ỳ Ǜ`

#### Mácron: ¯ (AltGr+\[)

{ Mnemônico: está próximo de outros acentos e do hífen. No Linux costuma vir na mesma posição }

Geralmente indica vogais longas. Utilizado em māori, japonês romanizado e diversas representações fonéticas, como em Latim.

`ā ē ḡ ī ō ū ǖ ȳ ǣ`  
`Ā Ē Ḡ Ī Ō Ū Ǖ Ȳ Ǣ`

#### Breve: ˘ (2016: AltGr+u – 2021: AltGr+b)

Geralmente indica vogais curtas, utilizado em diversas representações fonéticas, como em Latim. Em Esperanto, é usado em ŭ. O ğ é usado em turco.

`ă ĕ ğ ĭ ŏ ŭ`  
`Ă Ĕ Ğ Ĭ Ŏ Ŭ`

#### Cedilha: ¸ (AltGr+ç)

Agora você não está mais limitado ao C-cedilha!

`ç ḑ ȩ ģ ḩ ķ ļ ņ ŗ ş ţ`  
`Ç Ḑ Ȩ Ģ Ḩ Ķ Ļ Ņ Ŗ Ş Ţ`

#### Ogonek: ˛ (AltGr+Shift+Ç)

Utilizado em polonês, lituano e em outras línguas.

`ą ę į ǫ ų`  
`Ą Ę Į Ǫ Ų`

#### Vírgula embaixo: , (AltGr+Shift+,)

`ș ț`  
`Ș Ț`

#### Bolinha em cima: ˚ (AltGr+Shift+])

{ Mnemônico: está próximo aos símbolos ° (grau) e º (ordinal masculino) }

Em cima do A, utilizado em norueguês, dinamarquês, sueco e no símbolo de Ångström. Em cima do U, utilizado em tcheco.

`å ů ẘ ẙ`  
`Å Ů`

#### Ponto em cima (ponto ao lado no caso do L): ˙ (2016: AltGr+Shift+. – 2021: AltGr+Shift+L)

`ȧ ḃ ċ ḋ ė ḟ ġ ḣ   ŀ ṁ ṅ ȯ ṗ ṙ ṡ ṫ ẇ ẋ ẏ ż`  
`Ȧ Ḃ Ċ Ḋ Ė Ḟ Ġ Ḣ İ Ŀ Ṁ Ṅ Ȯ Ṗ Ṙ Ṡ Ṫ Ẇ Ẋ Ẏ Ż`

#### Acento agudo duplo: ˝ (AltGr+Shift+')

{ Mnemônico: as aspas " são semelhantes ao acento agudo duplo }

Utilizado em húngaro.

`ő ű`  
`Ő Ű`

#### Caron/Háček: ˇ (AltGr+v)

{ Mnemônico: o formato do ˇ é semelhante ao V }

Utilizado em tcheco, eslovaco, esloveno e em outras línguas.

`ǎ č ď ě ǧ ȟ ǐ ǰ ǩ ľ ň ǒ ř š ť ǔ ǚ ž ǯ`  
`Ǎ Č Ď Ě Ǧ Ȟ Ǐ   Ǩ Ľ Ň Ǒ Ř Š Ť Ǔ Ǚ Ž Ǯ`


#### Letra cortada: ĦŁŦ, etc. (2016: AltGr+', 2021: AltGr+Shift+X)

{ Disponível apenas no Windows }

`ƀ đ ǥ ħ ɨ ɉ ł ɍ ŧ ʉ ɏ ƶ`  
`Ƀ Đ Ǥ Ħ Ɨ Ɉ Ł Ɍ Ŧ Ʉ Ɏ Ƶ`  


### Símbolos fonéticos

O alfabeto fonético internacional contém mais símbolos do que seria possível adicionar ao teclado usando apenas AltGr, então escolhi alguns símbolos que também são usados como letras normais em outras línguas (portanto tendo versões maiúsculas e minúsculas) ou que sejam bastante comuns em dicionários de inglês ou francês, por exemplo. Alguns símbolos fonéticos podem ser obtidos por combinações mais complexas, por exemplo, utilizando o recurso de “letra cortada” descrito acima.

Abaixo, alguns exemplos das possibilidades (há outras, já que muitos símbolos fonéticos são letras dos alfabetos latino e grego).

#### Vogais

æ (AltGr+a)  
ə (AltGr+~)  
ɛ (AltGr+;) { e aberto }  
ı (AltGr+i)  
ɔ (AltGr+´) { o aberto }  
ø (AltGr+o)  
œ (AltGr+p)  

æ ə ɛ ı ɔ ø œ  
Æ Ə Ɛ   Ɔ Ø Œ

Obs.: os símbolos ə ɛ ɔ não estão disponíveis no layout para Linux

#### Consoantes

ʃ (AltGr+s) { representa "sh", x em português, e é o som do ŝ do Esperanto }  
ʒ (AltGr+g) { representa "zh", j em português, e é o som do ĵ do Esperanto }  
ŋ (AltGr+n)  
ð (AltGr+d) { um dos sons do "th" do inglês }  
ħ (2016: AltGr+' h, 2021: AltGr+Shift+X h, Linux: AltGr+h)  
θ (AltGr+Shift+U) { outro som do "th" do inglês }  

`ʃ ʒ ŋ ð ħ θ`  
`  Ʒ Ŋ Ð Ħ`

## Suporte a teclados não-brasileiros

O teclado brasileiro ABNT2 possui duas teclas adicionais próximas às teclas Shift↑ (tornando os Shift↑s mais curtos que no teclado estadunidense). Essas teclas contêm a barra invertida \\ e a barra inclinada /, junto com outros símbolos.

A tecla da barra invertida próxima ao Shift↑ esquerdo é comum em diversos teclados (como os europeus), sendo ausente apenas no teclado dos Estados Unidos. Mesmo assim, são oferecidas alternativas aos símbolos “\\” e “|”. A posição varia entre os leiautes 2016 e 2021. Um dos motivos da criação do layout 2021 era colocá-los mais próximos da sua posição correta no layout brasileiro, por isso foram escolhidas as teclas X e Z.

Já a tecla da barra inclinada /, interrogação ? e símbolo de grau ° é bem específica do teclado brasileiro, e é por isso que seus símbolos já são duplicados nas teclas Q, W, e E, respectivamente quando combinadas com AltGr. O símbolo adicionado neste layout, a interrogação invertida ¿, foi portanto adicionada em dois lugares, como AltGr+Shift+/ e AltGr+Shift+W.

## Suporte a Linux

O teclado brasileiro que vem em distribuições do GNU/Linux já possui vários destes símbolos, alguns em posições diferentes. Por exemplo, as flechinhas já estão nas teclas Y←, U↓↑, I→. Além disso, você pode ativar a tecla Compose para digitar várias combinações, como `Compose = /` que resulta em `≠` ou `Compose - - .` que resulta em `–` (meia-risca) ou `Compose - - -` que resulta em `—` (travessão) ou `Compose 1 2` que resulta em `½`. Eu costumo ativar a tecla Compose com a combinação AltGr junto com alguma tecla à direita da AltGr (como as teclas Win ou Menu).

Além disso, neste repositório há alguns arquivos de configuração para o XKB e Xmodmap com scripts prontos para usar, focando no leiaute de 2021, que pretende unificar o máximo possível as configurações do Windows e Linux. Você pode rodar os scripts na inicialização ou substituir os arquivos de sistema (exige cuidado, mas também evita que o layout reverta para o padrão ao colocar o computador para dormir ou alternar o console virtual com Ctrl+Alt+F1...F7).

No layout de 2021, optei por:
* Unificar as flechinhas em Y←, U↓↑, I→ tanto no Windows quanto no Linux.
* Coloquei os símbolos \, | nas teclas Z, X para facilitar ainda mais o uso de teclados americanos.
* Deixei mais fácil de usar os símbolos • e ✓, que se mostraram mais úteis que o esperado, principalmente para listas de tarefas. Agora eles usam apenas AltGr e não AltGr+Shift.
* S com ß e ʃ foi unificado entre Windows e Linux. Infelizmente tive que remover o Esh maiúsculo (Ʃ) no Windows, mas como ele é muito parecido com o Sigma grego e o sinal de somatório, estava confuso, já que no Unicode esses 3 símbolos são tecnicamente coisas diferentes (ainda que tenham praticamente a mesma aparência) e eu nunca tinha usado mesmo. O Esh maiúsculo no Linux pode ser digitado com CapsLock.
* ,< e .> com «» e ×÷. Os operadores matemáticos vêm direto do Linux, já as «aspas» são a minha posição preferida para esses símbolos há vários anos.

Diferenças entre Windows e Linux:
* Ŧŧ, Ħħ, Łł disponíveis diretamente no Linux, disponíveis com combinações no Windows. As teclas T, H, L e algumas outras (como P) portanto são ligeiramente diferentes entre os sistemas.
* Əə, Ɔɔ, Ɛɛ disponíveis apenas no Windows por causa da convenção no Linux em que AltGr+acento server para digitar rapidamente o acento sem letra em baixo. A posição ;: no Linux é usada para os símbolos ·˙ que no Windows estão no L.
* O Esh maiúsculo (Ʃ) no Linux pode ser digitado com CapsLock ligado e AltGr+Shift+S.

Possíveis melhorias:  
* Os símbolos ‹ e › são mais interessantes que ≈ e ≡ (AltGr+Shift+6 ou 7), mas teria que achar uma posição boa para o "parzinho" tanto no Windows quanto no Linux. No Windows a proposta é ficar no 6 e 7 mesmo, mas no Linux AltGr+Shift+6 já é usado para trema (¨) sem vogal em baixo.
* A letra ĸ (kra – possui somente minúscula) só era usada na ortografia antiga do groenlandês (foi substituído por q). Ele acabou entrando no layout 2021 porque já vinha no Linux e era uma letra meio óbvia para o AltGr+k. Mas quando eu descobri que nem em groenlandês ele é mais usado, passei a pensar em removê-lo ou pô-lo num lugar mais escondido (como AltGr+Shift+K).
* Se ʃ ficou sem maiúscula, ʒ talvez não precise de maiúscula também.
* Ponto em baixo poderia ser adicionado se sobrar algum espaço, de prererência próximo à tecla do ponto e de outros acentos.